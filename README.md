# Cheddar Framework
Funny smelling framework with several holes

## Testing

```bash
pecl run-tests -r -x --cgi=/usr/bin/php-cgi tests/
```

## Routing config

```php
<?php

return [
    Cheddar\Router\RouteMatcher::class => [
        [
            'route' => '/^\/$/',
            'action' => [DefaultController::class, 'index']
        ]
    ]
];
```

## Database config

```php
<?php

return [
    Cheddar\DBAL\Connection::class => [
        'dsn' => 'mysql:dbname=cheddar;host=127.0.0.1',
        'username' => $_ENV['DB_USERNAME'],
        'password' => $_ENV['DB_PASSWORD']
    ]
];

```

## Database schema config

```php
<?php

return [
    \Cheddar\DBAL\Schema::class => [
        Foo::class => [
            'table' => 'foo',
            'fields' => [
                'id' => [ 'type' => 'int' ],
                'bar_id' => [ 'type' => 'int' ]
            ],
            'relations' => [
                'bar' => [
                    'target' => Bar::class,
                    'local' => 'id',
                    'foreign' => 'bar_id',
                    'type' => 'one',
                    'cascade' => ['persist']
                ]
            ]
        ],
        Bar::class => [
            'table' => 'bar',
            'fields' => [
                'id' => [ 'type' => 'int' ],
                'baz'=> [ 'type' => 'text' ]
            ]
        ]
    ]
];
```
