<?php

namespace Cheddar\Router;

use Cheddar\Controller\OptionsController;
use Cheddar\Http\{RequestInterface, Exception\NotFoundException};

class RouteMatcher
{
    protected $routes;

    /**
     * 
     * @param array $_configuration
     */
    public function __construct(array $_configuration = [])
    {
        $this->routes = $_configuration;
    }
    
    protected function options(RequestInterface $request)
    {
        $methods = [];
        foreach($this->routes as $key => $route) {
            $matches = preg_match($route['route'], $request->getUri());
            if($matches && isset($route['methods'])) {
                $methods[] = $route['methods'];
            }
        }
        
        return new Route('@options', OptionsController::class, 'options', [
            'methods' => array_unique(array_merge(...$methods))
        ]);
    }
    
    /**
     * 
     * @param Request $request
     * @return \Cheddar\Router\Route|null
     */
    public function match(RequestInterface $request): ?Route
    {
        if($request->getMethod() === 'OPTIONS') {
            return $this->options($request);
        }
        
        foreach($this->routes as $key => $route) {
            if(isset($route['methods']) && !in_array($request->getMethod(), $route['methods'])) {
                // TODO: add method not allowed exception
                continue;
            }
            $params = [];
            $matches = preg_match($route['route'], $request->getUri(), $params);
            if($matches) {
                [$controller, $action] = $route['action'];
                $authRequired = $route['auth'] ?? false;
                return new Route($key, $controller, $action, $params, $authRequired);
            }
        }

        throw new NotFoundException(
            "No route found for request [{$request->getMethod()}] {$request->getUri()}"
        );
    }
    
}
