<?php

namespace Cheddar\Router;

use Cheddar\ParameterBag\ParameterBag;

class Route
{
    /**
     *
     * @var int|string
     */
    protected $route;
    
    /**
     *
     * @var string
     */
    protected $controller;
    
    /**
     *
     * @var string
     */
    protected $action;
    
    /**
     *
     * @var ParameterBag
     */
    protected $parameters;
    
    /**
     *
     * @var bool
     */
    protected bool $authRequired;


    /**
     * 
     * @param int|string $route
     * @param string $controller
     * @param string $action
     * @param array $parameters
     */
    public function __construct(
        $route, 
        string $controller,
        string $action,
        array $parameters,
        bool $authRequired = false
    )
    {
        $this->route = $route;
        $this->controller = $controller;
        $this->action = $action;
        $this->parameters = new ParameterBag($parameters);
        $this->authRequired = $authRequired;
    }
    
    /**
     * 
     * @return string|int
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * 
     * @return string
     */
    public function getController(): string
    {
        return $this->controller;
    }
    
    /**
     * 
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * 
     * @return ParameterBag|array
     */
    public function getParameters(): ParameterBag
    {
        return $this->parameters;
    }
    
    public function isAuthenticationRequired(): bool
    {
        return $this->authRequired;
    }
    
}
