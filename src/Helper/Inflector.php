<?php

namespace Cheddar\Helper;

class Inflector
{
    
    /**
     * Convert string from snake_case to lowerCamelCase
     * @param string $string
     * @return string
     */
    static public function camelize(string $string): string
    {
        $parts = explode('_', $string, 2);
        $head = $parts[0];
        $tail = ucwords($parts[1], '_');
        
        return $head . str_replace('_', '', $tail);
    }
    
}
