<?php

namespace Cheddar\DBAL;

interface RepositoryInterface
{
    
    public function find($id): ?object;
    
}
