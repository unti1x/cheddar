<?php

namespace Cheddar\DBAL;

class Query
{
    
    public const TYPE_SELECT = 'select';
    
    public const JOIN_LEFT = 'left';
    
    public const JOIN_INNER = 'inner';
    
    public const ORDER_ASC = 'ASC';
    
    public const ORDER_DESC = 'DESC';
    
    /**
     *
     * @var string
     */
    private $type;
    
    /**
     *
     * @var string
     */
    private $from;
    
    /**
     *
     * @var array
     */
    private $joins = [];
    
    /**
     *
     * @var string
     */
    private $conditions;
    
    /**
     *
     * @var int
     */
    private $limit = 0;
    
    /**
     *
     * @var array<string, string>
     */
    private $sort = [];
    
    /**
     *
     * @var array
     */
    private $parameters = [];
    
    /**
     *
     * @var int
     */
    private $skip = 0;

    public function select(string $entity): self
    {
        $this->type = self::TYPE_SELECT;
        $this->from = $entity;
        return $this;
    }
    
    public function with(string $relation, string $join = self::JOIN_LEFT): self
    {
        $this->joins[$relation] = $join;
        return $this;
    }
    
    public function filter(string $conditions): self
    {
        $this->conditions = $conditions;
        return $this;
    }
    
    public function limit(int $limit, int $skip = 0): self
    {
        $this->limit = $limit;
        $this->skip = $skip;
        return $this;
    }
    
    public function sortBy(string $field, string $direction = self::ORDER_ASC): self
    {
        if(!in_array($direction, [self::ORDER_ASC, self::ORDER_DESC])) {
            throw new \RuntimeException('Wrong ordering'); // FIXME: order exception
        }
        $this->sort[$field] = $direction; // FIXME: field name validation
        return $this;
    }
    
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Get main entity class name
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    public function getJoins(): array
    {
        return $this->joins;
    }

    public function getConditions(): ?string
    {
        return $this->conditions;
    }
    
    public function getLimit(): int
    {
        return $this->limit;
    }
    
    public function getSort(): array
    {
        return $this->sort;
    }

    public function getSkip(): int
    {
        return $this->skip;
    }

    public function getParameters(): array
    {
        return $this->parameters;
    }

    public function setParameters(array $parameters)
    {
        $this->parameters = $parameters;
        return $this;
    }

}
