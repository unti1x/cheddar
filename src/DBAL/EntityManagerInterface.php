<?php

namespace Cheddar\DBAL;

/**
 *
 * @author unti1x
 */
interface EntityManagerInterface
{
    /**
     * Entity is unknown by manager
     */
    public const STATUS_DETACHED = 0;
    
    /**
     * Entity is known and will be inserted on flush
     */
    public const STATUS_PERSISTED = 1;
    
    /**
     * Entity is known
     */
    public const STATUS_MANAGED = 2;
    
    /**
     * Entity is known and will be deleted from db on flush
     */
    public const STATUS_DELETED = 3;
 
    /**
     * 
     * @param object $entity
     * @return int
     */
    public function getStatus(object $entity): int;
    
    /**
     * 
     * @param object $entity
     * @return \self
     */
    public function persist(object $entity): self;
    
    /**
     * 
     * @param object $entity
     * @return void
     */
    public function delete(object $entity): void;
    
    /**
     * 
     * @return void
     */
    public function flush(): void;
    
    /**
     * 
     * @param object $entity
     * @return void
     */
    public function attach(object $entity): void;
    
    /**
     * 
     * @param object $entity
     * @return void
     */
    public function detach(object $entity): void;

}
