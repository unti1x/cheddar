<?php

namespace Cheddar\DBAL;

class QuerySchema
{
    /**
     *
     * @var string
     */
    protected $entity;
    
    /**
     *
     * @var string
     */
    protected $table;
    
    /**
     *
     * @var string
     */
    protected $conditions;

    /**
     *
     * @var int
     */
    protected $limit = 0;
    
    /**
     *
     * @var int
     */
    protected $skip = 0;

    /**
     *
     * @var array
     */
    protected $sort = [];


    /**
     *
     * @var array
     */
    protected $fields = [];
    
    /**
     * Array of mapped request columns to entity fields.
     * ['relation_name' => ['field' => 'column_number_or_identifier']]
     * @var array
     */
    protected $fieldsMap = [];

    /**
     *
     * @var array
     */
    protected $relations = [];
    
    
    public function getEntity(): ?string
    {
        return $this->entity;
    }

    public function setEntity(string $entity): self
    {
        $this->entity = $entity;
        return $this;
    }
        
    public function getTable(): ?string
    {
        return $this->table;
    }
    
    public function setTable(string $table): self
    {
        $this->table = $table;
        return $this;
    }

    public function getConditions(): ?string
    {
        return $this->conditions;
    }

    public function setConditions(?string $conditions): self
    {
        $this->conditions = $conditions;
        return $this;
    }    
    
    public function getLimit(): int
    {
        return $this->limit;
    }

    public function setLimit(int $limit): self
    {
        $this->limit = $limit;
        return $this;
    }

    public function getSkip(): int
    {
        return $this->skip;
    }

    public function setSkip(int $skip): self
    {
        $this->skip = $skip;
        return $this;
    }
    
    public function setSort($sort): self
    {
        $this->sort = $sort;
        return $this;
    }
    
    public function getFieldsMap(): array
    {
        return $this->fieldsMap;
    }

    public function getRelations(): array
    {
        return $this->relations;
    }

    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * 
     * @param array $fields
     * @return \self
     */
    public function setFields(array $fields): self
    {
        $this->fields = array_map(function($field) {
            return "`{$this->table}`.`$field`";
        }, $fields);
        
        $this->fieldsMap['_self'] = array_flip($fields);
        
        return $this;
    }
    
    /**
     * Add joined fields with relation prefix
     * 
     * @param string $relation Relation name
     * @param array $fields Related entity fields
     * @return \self
     */
    public function addFields(string $relation, array $fields): self
    {
        $index = count($this->fields);
        $this->fieldsMap[$relation] = [];
        
        foreach($fields as $field) {
            $this->fields[$index] = "`{$relation}`.`$field`";
            $this->fieldsMap[$relation][$field] = $index++;
        }
        
        return $this;
    }
    
    /**
     * Add relation and related fields
     * 
     * @param string $name Relation name
     * @param string $table Relating entity table
     * @param string $join Relation join type. See Query::JOIN_* constants
     * @param array $relation Relation configuration
     * @param array $fields Related entity fields
     * @return \self
     */
    public function addRelation(string $name, string $table, string $join, array $relation, array $fields): self
    {
        $relation['table'] = $table; // FIXME
        $relation['join'] = $join; // FIXME
        $this->relations[$name] = $relation;
        $this->addFields($name, $fields);
        
        return $this;
    }
    
    /**
     * Compile relations query
     * 
     * @return array
     */
    private function getJoins(): array
    {
        $joins = [];
        
        foreach($this->relations as $name => $relation) {
            $table = $relation['table'];
            $join = (
                (isset($relation['join']) && $relation['join'] === 'left')
                ? 'LEFT' 
                : 'INNER'
            ) . ' JOIN';
            $foreign = "`$name`.`{$relation['foreign']}`";
            $local = "`{$this->table}`.`{$relation['local']}`";
            
            $joins[] = "$join `$table` `$name` ON $foreign = $local";
        }
        
        return $joins;
    }
    
    private function getOrderBy(): string
    {
        $orderBy = [];
        
        foreach($this->sort as $key => $order) {
            $orderBy[] = "$key $order";
        }
        
        return join(',', $orderBy);
    }
    
    /**
     * Compile SELECT-query
     * 
     * @return string
     */
    private function getSelectQuery(): string
    {
        $fields = join(', ', $this->fields);
        $joinsPart = join(' ', $this->getJoins());
        $conditionsPart = $this->conditions ? "WHERE {$this->conditions}" : '';
        $offsetPart = $this->skip > 0 ? "OFFSET {$this->skip}" : '';
        $limitPart = $this->limit > 0 ? ( "LIMIT {$this->limit} $offsetPart" ) : '';
        $orderPart = count($this->sort) ? "ORDER BY {$this->getOrderBy()}" : '';

        return "SELECT $fields FROM `{$this->table}` $joinsPart $conditionsPart $orderPart $limitPart";
    }
    
    /**
     * Get sql query string
     * @todo add more query types
     * @return string
     */
    public function getSql(): string
    {
        return $this->getSelectQuery();
    }
}
