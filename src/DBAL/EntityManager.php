<?php

namespace Cheddar\DBAL;

use Cheddar\Helper\Inflector;

class EntityManager implements EntityManagerInterface
{

    /**
     *
     * @var Schema
     */
    protected $schema;
    
    /**
     *
     * @var array
     */
    protected $entities = [];
    
    /**
     *
     * @var array
     */
    protected $newEntities = [];
    
    /**
     *
     * @var array
     */
    protected $deletedEntities = [];

    /**
     * 
     * @param \Cheddar\DBAL\Schema $schema
     */
    public function __construct(Schema $schema)
    {
        $this->schema = $schema;
    }
        
    /**
     * Get entity management status. See EntityManager::STATUS_* constants
     * @param object $entity
     * @return int
     */
    public function getStatus(object $entity): int
    {
        $objId = spl_object_hash($entity);
        $status = self::STATUS_DETACHED;
        
        if(isset($this->entities[$objId])) {
            $status = self::STATUS_MANAGED;
            
        } elseif(isset($this->newEntities[$objId])) {
            $status = self::STATUS_PERSISTED;
            
        } elseif(isset($this->deletedEntities[$objId])) {
            $status = self::STATUS_DELETED;
        }
        
        return $status;
    }
    
    /**
     * 
     * @param object|iterable|null $related
     * @return void
     * @throws \RuntimeException
     */
    protected function persistRelatedObjects($related): void
    {
        if(empty($related)) {
            return;
        }

        if(is_object($related)) {
            $this->persist($related);
            
        } elseif(is_array($related)) {
            foreach($related as $entity) {
                $this->persistRelatedObjects($entity);
            }
            
        } else {
            // FIXME
            throw new \RuntimeException('Couldn\'t persist scalar related objects');
        }
    }


    /**
     * Recoursively persisting relations with cascade config
     * @param object $entity
     * @param array $relations
     * @return void
     */
    protected function persistRelations(object $entity, array $relations): void
    {
        foreach($relations as $relation => $config) {
            if(isset($config['cascade']) && in_array('persist', $config['cascade'])) {
                $getter = Inflector::camelize("get_$relation");
                //TODO: throw method not found
                $related = $entity->$getter();
                $this->persistRelatedObjects($related);
            }
        }
    }
    
    public function persist(object $entity): self
    {
        if($this->getStatus($entity) === self::STATUS_DETACHED) {
            $objId = spl_object_hash($entity);
            $this->newEntities[$objId] = $entity;

            $entityClass = get_class($entity);
            $config = $this->schema->get($entityClass);

            if(isset($config['relations'])) {
                $this->persistRelations($entity, $config['relations']);
            }
        }

        return $this;
    }
    
    public function delete(object $entity): void
    {
        $objId = spl_object_hash($entity);
        if($this->getStatus($entity) === self::STATUS_MANAGED) {
            unset($this->entities[$objId]);
            $this->deletedEntities[$objId] = $entity;
        }
    }
    
    /**
     * Process all db changes
     * @todo implement me
     * @return void
     */
    public function flush(): void
    {
        ;
    }
    
    /**
     * Start managing an entity
     * @param object $entity
     * @return void
     */
    public function attach(object $entity): void
    {
        $objId = spl_object_hash($entity);
        $this->entities[$objId] = $entity;
    }
    
    /**
     * Stop managing an entity
     * @param object $entity
     * @return void
     */
    public function detach(object $entity): void
    {
        $objId = spl_object_hash($entity);
        switch ($this->getStatus($entity)) {
            case self::STATUS_MANAGED:
                unset($this->entities[$objId]);
                break;
            case self::STATUS_PERSISTED:
                unset($this->newEntities[$objId]);
                break;
            case self::STATUS_DELETED:
                unset($this->deletedEntities[$objId]);
                break;
        }
    }

}
