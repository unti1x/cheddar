<?php

namespace Cheddar\DBAL\Exception;

/**
 *
 * @author unti1x
 */
class EntityNotConfiguredException extends \RuntimeException
{
    
    protected $message = 'The entity class has not been configured or does not exist';
    
}
