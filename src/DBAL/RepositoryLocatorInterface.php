<?php

namespace Cheddar\DBAL;

/**
 *
 * @author unti1x
 */
interface RepositoryLocatorInterface
{
        
    /**
     * 
     * @param string $className
     * @return \Cheddar\DBAL\RepositoryInterface
     */
    public function getRepository(string $className): RepositoryInterface;
}
