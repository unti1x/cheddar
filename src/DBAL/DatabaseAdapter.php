<?php

namespace Cheddar\DBAL;

class DatabaseAdapter
{
    /**
     *
     * @var Schema
     */
    protected $schema;
    
    /**
     *
     * @var Hydrator
     */
    protected $hydrator;
    
    /**
     *
     * @var Connection
     */
    protected $connection;

    /**
     * 
     * @param \Cheddar\DBAL\Connection $connection
     * @param \Cheddar\DBAL\Schema $schema
     * @param \Cheddar\DBAL\Hydrator $hydrator
     */
    public function __construct(Connection $connection, Schema $schema, Hydrator $hydrator)
    {
        $this->connection = $connection;
        $this->schema = $schema;
        $this->hydrator = $hydrator;
    }
    
    /**
     * 
     * @param \Cheddar\DBAL\Query $query
     * @return \Cheddar\DBAL\QuerySchema
     */
    protected function prepareQuery(Query $query): QuerySchema
    {
        $tableSchema = $this->schema->get($query->getFrom());
        
        $querySchema = new QuerySchema();
        $querySchema
            ->setEntity($query->getFrom())
            ->setTable($tableSchema['table'])
            ->setConditions($query->getConditions())
            ->setSort($query->getSort())
            ->setLimit($query->getLimit())
            ->setSkip($query->getSkip());
        
        $fields = array_keys($tableSchema['fields']);
        $querySchema->setFields($fields);
        
        foreach($query->getJoins() as $relation => $join) {
            $relationField = $tableSchema['relations'][$relation];
            // TODO: throw new RelationNotDefined
            
            $relationSchema = $this->schema->get($relationField['target']);
            
            $querySchema->addRelation(
                $relation, 
                $relationSchema['table'],
                $join,
                $relationField,
                array_keys($relationSchema['fields'])
            );
        }
        
        return $querySchema;
    }
    
    /**
     * 
     * @param \Cheddar\DBAL\Query $query
     * @return iterable|null
     */
    public function executeQuery(Query $query): ?iterable
    {
        
        $schema = $this->prepareQuery($query);
        $sql = $schema->getSql();
        $rawData = $this->connection->execute($sql, $query->getParameters());
        $result = $this->hydrator->hydrate($schema, $rawData);

        return $result;
    }
    
    /**
     * 
     * @param string $sql
     * @param array $params
     * @param bool $returnResult
     * @return mixed
     */
    public function executeSql(string $sql, array $params = [], bool $returnResult = false)
    {
        $result = $this->connection->execute($sql, $params, $returnResult);
        if(strpos($sql, 'INSERT') === 0 && $returnResult) {
            $result = $this->connection->getLastId();
        }
        
        return $result;
    }
    
}
