<?php

namespace Cheddar\DBAL;

use Cheddar\DBAL\Exception\QueryFailedException;

class Connection
{
    /**
     *
     * @var \PDO
     */
    protected $connection;
    
    /**
     * 
     * @param array $_configuration
     */
    public function __construct(array $_configuration)
    {
        $this->connection = new \PDO(
            $_configuration['dsn'], 
            $_configuration['username'],
            $_configuration['password']
        );
    }
    
    /**
     * 
     * @param string $sql
     * @param array $parameters
     * @param bool $returnResult
     * @return type
     */
    public function execute(string $sql, array $parameters = [], bool $returnResult = true)
    {
        $query = $this->connection->prepare($sql);
        foreach($parameters as $key => &$value) {
            $param = is_array($value) ? ("'" . (join("','", $value)) . "'" ) : $value;
            $query->bindValue($key, $param);
        }
        
        $success = $query->execute();
        
        if(!$success) {
            [$state, $driverCode, $message] = $query->errorInfo();
            
            throw new QueryFailedException(
                "Query failed: $sql; [{$state}:{$driverCode}] {$message}."
            );
        }
        
        if($returnResult) {
            return $query->fetchAll();
        }
    }

    /**
     * 
     * @return mixed
     */
    public function getLastId()
    {
        return $this->connection->lastInsertId();
    }
    
}
