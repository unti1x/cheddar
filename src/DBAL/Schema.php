<?php

namespace Cheddar\DBAL;

use Cheddar\ParameterBag\ParameterBag;

class Schema extends ParameterBag
{
    
    public function __construct(array $_configuration)
    {
        parent::__construct($_configuration);
    }
    
}
