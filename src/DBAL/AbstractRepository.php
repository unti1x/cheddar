<?php

namespace Cheddar\DBAL;

class AbstractRepository implements RepositoryInterface
{
    
    /**
     *
     * @var EntityManager
     */
    private $em;
    
    /**
     *
     * @var DatabaseAdapter
     */
    private $adapter;

    /**
     *
     * @var string
     */
    private $entity;
    
    public function __construct(EntityManager $em, DatabaseAdapter $adapter)
    {
        $this->em = $em;
        $this->adapter = $adapter;
    }
    
    protected function executeSql(string $query, array $params = [])
    {
        return $this->adapter->executeSql($query, $params, true);
    }
    
    protected function setEntity(string $entity)
    {
        $this->entity = $entity;
    }
    
    protected function getQuery(): Query
    {
        return (new Query())->select($this->entity);
    }
    
    protected function getResult(Query $query): ?iterable
    {
        $result = $this->adapter->executeQuery($query);
        if(is_array($result)) {
            foreach($result as $entity) {
                $this->em->attach($entity);   
            }
        }
        
        return $result;
    }
    
    protected function getSingleResult(Query $query): ?object
    {
        $result = $this->adapter->executeQuery($query);

        if(is_array($result) && !empty($result)) {
            $entity = array_shift($result);
            $this->em->attach($entity);
            
        } else {
            $entity = $result;
            if(is_object($entity)) {
                $this->em->attach($entity);
            }
        }
        
        return empty($entity) ? null : $entity;
    }
    
    public function find($id): ?object
    {
        $query = $this->getQuery()
            // TODO: getTableName() + getIndexField()
            ->filter('`id` = :id')
            ->limit(1)
            ->setParameters([':id' => $id]);
        
        return $this->getSingleResult($query);
    }
    
    public function findAll(): array
    {
        $query = $this->getQuery();
        return $this->getResult($query);
    }
    
}
