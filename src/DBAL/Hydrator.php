<?php

namespace Cheddar\DBAL;

use Cheddar\Helper\Inflector;

class Hydrator
{
    /**
     *
     * @var Schema
     */
    protected $schema;

    protected $instantCache = [];
    
    public function __construct(Schema $schema)
    {
        $this->schema = $schema;
    }
    
    /**
     * Unflatten result 
     * @param \Cheddar\DBAL\QuerySchema $schema
     * @param iterable $rawData
     * @return array
     */
    protected function unzipData(QuerySchema $schema, iterable $rawData): array
    {
        $data = [];
        $fieldsMap = $schema->getFieldsMap();
        
        foreach($fieldsMap as $key => $fields) {
            $data[$key] = [];
            foreach ($fields as $field => $column) {
                $data[$key][$field] = $rawData[$column];
            }
        }
        
        return $data;
    }

    /**
     * Merge similar results
     * @param iterable $data
     * @return \Cheddar\DBAL\arrya
     */
    protected function windData(QuerySchema $schema, iterable $rawData): array
    {
        $data = [];
        $relSchema = $this->schema->get($schema->getEntity())['relations'] ?? []; // FIXME

        foreach($rawData as $item) {
            $id = $item['_self']['id']; // FIXME
            if(!isset($data[$id])) {
                $data[$id] = [ '_self' => $item['_self'] ];
            }
            
            unset($item['_self']);
            
            foreach($item as $relation => $columns) {
                $relIdField = $relSchema[$relation]['foreign'];
                $relId = $columns[$relIdField];
                
                if($relId !== null) {
                    $data[$id][$relation][$relId] = $columns;
                }
            }
        }
        
        return $data;
    }
    
    /**
     * Create entity and fill it with data
     * 
     * @param string $class
     * @param array $data
     * @return object
     */
    protected function soakEntity(string $class, array $data): object
    {
        $id = $data['id'];
        if(!isset($this->instantCache[$class][$id])) {
            $entity = new $class();
            foreach ($data as $field => $value) {
                $setter = Inflector::camelize("set_$field");

                if(method_exists($entity, $setter)) {
                    call_user_func([$entity, $setter], $value);
                }
            }

            $this->instantCache[$class][$id] = $entity;
        }
        
        return $this->instantCache[$class][$id];
    }
    
    protected function setRelatedObjects(object $entity, string $key, array $objects, array $relations)
    {

        $class = $relations[$key]['target'];
        $prefix = $relations[$key]['type'] === 'one' ? 'set' : 'add';
        // TODO: add repluralisation method
        
        foreach ($objects as $object) {
            $relation = $this->soakEntity($class, $object);
            $setter = Inflector::camelize("{$prefix}_$key");
            call_user_func([$entity, $setter], $relation);
        }

    }
    
    /**
     * 
     * @param \Cheddar\DBAL\QuerySchema $schema
     * @param array $dataRow
     * @return object
     */
    protected function getEntity(QuerySchema $schema, array $dataRow): object
    {
        $relations = $schema->getRelations();
        $entity = null;
        
        foreach ($dataRow as $key => $fields) {
            if($key === '_self') {
                $entity = $this->soakEntity($schema->getEntity(), $fields);
            } else {
                $this->setRelatedObjects($entity, $key, $fields, $relations);
            }
        }
        
        return $entity;
    }

    /**
     * 
     * @param \Cheddar\DBAL\QuerySchema $schema
     * @param iterable $rawData
     * @return array
     */
    public function hydrate(QuerySchema $schema, iterable $rawData): array
    {
        $this->instantCache = [];
        $unzippedData = [];
        foreach($rawData as $line) {
            $unzippedData[] = $this->unzipData($schema, $line);
        }
        
        $data = $this->windData($schema, $unzippedData);
        
        $entities = array_map(function(array $data) use ($schema) {
            return $this->getEntity($schema, $data);
        }, $data);
        
        $this->instantCache = [];
        
        return $entities;
    }
    
}
