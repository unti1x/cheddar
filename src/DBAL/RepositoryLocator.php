<?php

namespace Cheddar\DBAL;

use Cheddar\DBAL\Exception\ModelNotConfiguredException;
use Cheddar\DBAL\SimpleRepository;

/**
 * Description of RepositoryLocator
 *
 * @author unti1x
 */
class RepositoryLocator implements RepositoryLocatorInterface
{
    
    protected Schema $schema;
    
    protected EntityManagerInterface $em;
    
    protected DatabaseAdapter $adapter;

    protected array $repositories = [];

    public function __construct(Schema $schema, EntityManager $em, DatabaseAdapter $adapter)
    {
        $this->schema = $schema;
        $this->em = $em;
        $this->adapter = $adapter;
    }
    
    /**
     * 
     * @param string $className
     * @return \Cheddar\DBAL\RepositoryInterface
     */
    public function getRepository(string $className): RepositoryInterface
    {
        $config = $this->schema->get($className);
        
        if(!$config) {
            throw new ModelNotConfiguredException();
        }
        
        $repo = $this->repositories[$className] ?? null;
        if(!$repo) {
            $repoName = $config['repository'] ?? null;

            if($repoName && class_exists($repoName) && is_subclass_of($repoName, AbstractRepository::class)) {
                $repo = new $repoName($this->em, $this->adapter);

            } else {
                $repo = new SimpleRepository($this->em, $this->adapter);
                $repo->setEntityClass($className);

            }

            $this->repositories[$className] = $repo;
        }
        

        return $repo;
    }
    
}
