<?php

namespace Cheddar\DBAL;

/**
 *
 * @author unti1x
 * @todo implement findBy
 */
class SimpleRepository extends AbstractRepository
{
    /**
     * @todo make something better
     * @param string $className
     * @return void
     */
    public function setEntityClass(string $className): void
    {
        $this->setEntity($className);
    }
}
