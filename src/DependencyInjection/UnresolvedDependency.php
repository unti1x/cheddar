<?php

namespace Cheddar\DependencyInjection;

use Psr\Container\ContainerExceptionInterface;

class UnresolvedDependency extends \RuntimeException implements ContainerExceptionInterface
{
    
}
