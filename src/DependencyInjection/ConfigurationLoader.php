<?php

namespace Cheddar\DependencyInjection;

use Cheddar\ParameterBag\ParameterBag;

class ConfigurationLoader
{
    
    static public function load(string $path): ParameterBag
    {
        $parameters = [];
        $files = glob($path, GLOB_NOSORT);
        
        foreach($files as $file) {
            $parameters[] = require($file);
        }
        
        $config = array_merge(...$parameters);
        
        return new ParameterBag($config);
    }
    
}
