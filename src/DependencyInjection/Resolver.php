<?php

namespace Cheddar\DependencyInjection;

class Resolver
{
    /**
     *
     * @var Container
     */
    protected $container;

    /**
     * 
     * @param \Cheddar\DependencyInjection\Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }
    
    /**
     * 
     * @param string $class
     * @param iterable $options
     * @return object
     */
    public function getInstance(string $class, iterable $options = []): object
    {
        $ref = new \ReflectionClass($class);
        $constructor = $ref->getConstructor();
        $args = $constructor ? $this->getArguments($constructor, $options) : [];
        $instance = new $class(...$args);
        
        return $instance;
    }


    /**
     * 
     * @param \ReflectionParameter $param
     * @param array|\ArrayAccess $options
     * @return mixed
     * @throws UnresolvedDependency
     */
    protected function resolveParameter(\ReflectionParameter $param, $options)
    {
        $class = $param->getClass();
        $name = $param->getName();
        $arg = null;
        
        if($name === '_configuration') {
            $arg = $this->container->getConfig($param->getDeclaringClass()->getName());

        } else if($class) {
            
            if(!$this->container->has($class->getName())) {
                $this->container->set($class->getName(), $class->getName());
            }
            
            $dep = $this->container->get($class->getName());
            $arg = is_string($dep) ? $this->getInstance($dep) : $dep;
            
        } elseif(!$class && isset($options[$name])) {
           $arg = $options[$name];

        } elseif($param->isOptional()) {
            $arg = $param->getDefaultValue();
        }
        
        if($arg) {
            if(is_object($arg)) {
                $this->container->set($name, $arg);
            }
            return $arg;
        } else {
            $className = $class ? $class->getName() : 'null';
            throw new UnresolvedDependency(
                "Couldn't resolve parameter `$name` of type `$className` of "
                . "{$param->getDeclaringClass()->getName()}::{$param->getDeclaringFunction()->getName()}"
            );
        }
    }
    
    /**
     * 
     * @param \ReflectionMethod $method
     * @param array|\ArrayAccess $options
     * @return mixed
     */
    public function getArguments(\ReflectionMethod $method, $options)
    {
        $paramsRef = $method->getParameters();
        
        $args = array_map(function(\ReflectionParameter $param) use ($options) {
            return $this->resolveParameter($param, $options);
        }, $paramsRef);
        
       return $args;
    }
    
}
