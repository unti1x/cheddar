<?php
namespace Cheddar\DependencyInjection;

use Psr\Container\ContainerInterface;
use Cheddar\ParameterBag\ParameterBag;

class Container implements ContainerInterface
{
    /**
     *
     * @var ParameterBag
     */
    private $parameters;
    
    /**
     *
     * @var ParameterBag
     */
    private $services;
    
    /**
     *
     * @var ParameterBag
     */
    private $config;
    
    /**
     *
     * @var Resolver
     */
    private $resolver;

    /**
     * 
     * @param \Cheddar\DependencyInjection\ParameterBag $config
     */
    public function __construct(ParameterBag $config)
    {
        $this->services = new ParameterBag($config->get('services'));
        $this->parameters = new ParameterBag($config->get('parameters'));
        $this->config = $config;            
    }
    
    /**
     * 
     * @param \Cheddar\DependencyInjection\Resolver $resolver
     * @return void
     */
    public function setResolver(Resolver $resolver): void
    {
        $this->resolver = $resolver;
        $this->set(Resolver::class, $resolver);
    }
    
    /**
     * 
     * @param string $id
     * @return object
     * @throws UnresolvedDependency
     */
    public function get($id)
    {
        if($this->has($id)) {
            $item = $this->services->get($id);
        } else {
            throw new UnresolvedDependency("Couldn't resolve dependency $id");
        }
        
        if(is_string($item)) {
            $object = $this->resolver->getInstance($item);
            $this->set($item, $object);
            $this->set($id, $object);
            
            $result = $object;
        } else {
            $result = $item;
        }
        
        return $result;
    }

    /**
     * 
     * @param string $id
     * @return bool
     */
    public function has($id): bool
    {
        return $this->services->offsetExists($id);
    }

    /**
     * 
     * @param string $id
     * @param object|string $value
     * @return void
     */
    public function set(string $id, $value): void
    {
        $this->services[$id] = $value;
        
        if(is_object($value)) {
            $class = get_class($value);
            if($id !== $class) {
                $this->services[$class] = $value;
            }
        }
    }
    
    public function getConfig($name)
    {
        return $this->config->get($name);
    }
    
}
