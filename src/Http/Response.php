<?php

namespace Cheddar\Http;

class Response implements ResponseInterface
{

    protected $headers;
    
    /**
     *
     * @var mixed
     */
    protected $body;
    
    /**
     *
     * @var int
     */
    protected $code;
    
    /**
     * 
     * @param mixed $body
     * @param array $headers
     */
    public function __construct($body = null, array $headers = [], int $code = 200)
    {
        $this->body = $body;
        $this->headers = $headers;
        $this->code = $code;
    }
    
    /**
     * 
     * @return string
     */
    public function getContent(): string
    {
        return $this->body;
    }
    
    /**
     * 
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }
    
    public function getCode(): int
    {
        return $this->code;
    }
    
}