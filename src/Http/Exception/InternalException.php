<?php

namespace Cheddar\Http\Exception;

class InternalException extends HttpException
{
    protected $code = 500;
    
    protected $message = 'Internal Server Error';
}
