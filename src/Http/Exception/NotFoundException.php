<?php

namespace Cheddar\Http\Exception;

class NotFoundException extends HttpException
{
    protected $code = 404;
}
