<?php

namespace Cheddar\Http;

use Cheddar\ParameterBag\ParameterBag;

class Request implements RequestInterface
{

    /**
     *
     * @var string
     */
    protected $uri;

    /**
     *
     * @var string
     */
    protected $method;


    /**
     * @var ParameterBag
     */
    protected $query;
    
    /**
     * @var ParameterBag
     */
    protected $request;
    
    /**
     * @var ParameterBag
     */
    protected $server;
    
    /**
     * @var ParameterBag
     */
    protected $headers;
    
    /**
     * 
     * @param array $server
     * @return array
     */
    private function getRequestHeaders(array $server): array
    {
        $headers = [];
        $contentHeaders = ['CONTENT_LENGTH' => true, 'CONTENT_MD5' => true, 'CONTENT_TYPE' => true];
        foreach ($server as $key => $value) {
            if (0 === strpos($key, 'HTTP_')) {
                $headers[substr($key, 5)] = $value;
            }

            elseif (isset($contentHeaders[$key])) {
                $headers[$key] = $value;
            }
        }
        
        return $headers;
    }
    
    /**
     * 
     * @param array $post
     * @return array
     */
    private function getRequestBody(array $post): array
    {
        $method = $this->server->get('REQUEST_METHOD');
        $contentType = $this->headers->get('CONTENT_TYPE');
        $isSerializedBody = in_array($method, ['PUT', 'DELETE', 'PATCH'])
            || strpos($contentType, 'application/json') === 0;
        
        return $isSerializedBody
            ? (json_decode(file_get_contents('php://input'), true) ?: [])
            : $post;
    }
    
    public function __construct(array $get, array $post, array $server)
    {
        $this->query = new ParameterBag($get);
        $this->server = new ParameterBag($server);
        $this->headers = new ParameterBag($this->getRequestHeaders($server));
        $this->request = new ParameterBag($this->getRequestBody($post));
        $queryString = $this->server->get('REQUEST_URI');
        $this->uri = explode('?', $queryString, 2)[0];
        $this->method = $this->server->get('REQUEST_METHOD');
    }

    public function getQuery(): ParameterBag
    {
        return $this->query;
    }

    public function getRequest(): ParameterBag
    {
        return $this->request;
    }

    public function getServer(): ParameterBag
    {
        return $this->server;
    }

    public function getHeaders(): ParameterBag
    {
        return $this->headers;
    }

    public function getController(): ?string
    {
        return $this->controller;
    }
    
    public function getUri(): string
    {
        return $this->uri;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

}