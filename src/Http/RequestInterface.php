<?php

namespace Cheddar\Http;

use Cheddar\ParameterBag\ParameterBag;

interface RequestInterface
{
    
    /**
     * Get query string parameter collection
     * @return ParameterBag
     */
    public function getQuery(): ParameterBag;
    
    /**
     * Get request body parameter collection
     * @return ParameterBag
     */
    public function getRequest(): ParameterBag;
    
    /**
     * Get contents of $_SERVER variable
     * @return ParameterBag
     */
    public function getServer(): ParameterBag;
    
    /**
     * Get request headers
     * @return ParameterBag
     */
    public function getHeaders(): ParameterBag;
    
    /**
     * Get resolved controller name if any
     * @return string|null
     */
    public function getController(): ?string;
    
    /**
     * Get request URI
     * @return string
     */
    public function getUri(): string;
    
    /**
     * Get request method
     * @return string
     */
    public function getMethod(): string;
    
}
