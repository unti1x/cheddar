<?php

namespace Cheddar\Http;

/**
 * JsonResponse
 *
 * @author unti1x
 */
class JsonResponse extends Response
{
    
    public function getHeaders(): array
    {
        return array_merge($this->headers, [
            'Content-Type' => 'application/json'
        ]);
    }
    
    public function getContent(): string
    {
        return json_encode($this->body);
    }
    
}
