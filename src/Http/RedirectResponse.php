<?php

namespace Cheddar\Http;


class RedirectResponse implements ResponseInterface
{

    protected $code;

    protected $headers;

    protected $location;

    /**
     * 
     * @param int $code
     * @param array $headers
     */
    public function __construct(
        string $location,
        int $code = self::HTTP_FOUND, 
        array $headers = []
    )
    {
        $this->location = $location;
        $this->code = $code;
        $this->headers = $headers;
    }
    
    public function getCode(): int
    {
        return $this->code;
    }

    public function getContent(): string
    {
        return '';
    }

    public function getHeaders(): array
    {
        return [
            'Location' => $this->location,
            ...$this->headers
        ];
    }

    
}
