<?php

namespace Cheddar\Http;

/**
 *
 * @author unti1x
 */
interface ResponseInterface
{
    public const HTTP_OK = 200;
    
    public const HTTP_NO_CONTENT = 204;
    
    public const HTTP_NOT_FOUND = 404;
    
    public const HTTP_INTERNAL_ERROR = 500;
    
    public const HTTP_MOVED_PERMANENTLY = 301;
    
    public const HTTP_FOUND = 302;

    /**
     * Get response data
     * @return string
     */
    public function getContent(): string;
    
    /**
     * Get response headers
     * @return array
     */
    public function getHeaders(): array;

    
    /**
     * Get responce HTTP status code
     * @return int
     */
    public function getCode(): int;
    
}
