<?php
namespace Cheddar;

use Cheddar\ParameterBag\ParameterBag;
use Cheddar\Router\RouteMatcher;
use Cheddar\Controller\ControllerResolver;
use Cheddar\Http\Exception\HttpException;
use Cheddar\Security\{Guard, GuardInterface};
use Cheddar\Security\Exception\AuthenticationRequiredException;
use Cheddar\DependencyInjection\{Container, Resolver};
use Cheddar\Http\{RequestInterface, ResponseInterface, Request, Response};

abstract class Kernel
{
    
    /**
     *
     * @var Container
     */
    protected $container;
    
    /**
     *
     * @var ParameterBag
     */
    protected $parameters;
    
    /**
     *
     * @var string
     */
    protected $baseDir;
    
    /**
     * 
     * @return string
     */
    protected function getBaseDir(): string
    {
        $ref = new \ReflectionClass($this);
        $path = $ref->getFileName();
        return dirname($path) . '/..';
    }
    
    /**
     * Load configuration files using 
     * {@see Cheddar\DependencyInjection\ConfigurationLoader} 
     */
    abstract protected function loadParameters();

    /**
     * Initialise container, base dir and load configuration
     */
    public function boot(): void
    {
        $this->parameters = new ParameterBag();
        $this->baseDir = $this->getBaseDir();
        $this->loadParameters();
        $this->container = new Container($this->parameters);
        $resolver = new Resolver($this->container);
        $this->container->setResolver($resolver);
        $this->container->set(Container::class, $this->container);
        $this->container->set('@router', RouteMatcher::class);
        $this->container->set('@controller_resolver', ControllerResolver::class);
        $this->container->set('@guard', Guard::class);
        $this->container->set(GuardInterface::class, Guard::class);
    }
    
    private function doHandleRequest(Request $request)
    {
        $this->container->set(Request::class, $request);

        /* @var $router RouteMatcher */
        $router = $this->container->get('@router');
        $route = $router->match($request);

        /* @var $guard Guard */
        $guard = $this->container->get('@guard');
        $guard->start();
        $user = $guard->getUser();
        $this->container->set('@user', $user);
        
        if($route->isAuthenticationRequired() && !$user) {
            throw new AuthenticationRequiredException();
        }

        /* @var $resolver Controller\ControllerResolver */
        $resolver = $this->container->get('@controller_resolver');

        $action = $resolver->resolve($route);
        return call_user_func_array(...$action);
    }
    
    /**
     * 
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function handleRequest(RequestInterface $request): ResponseInterface
    {
        try {
            ob_start();
            $response = $this->doHandleRequest($request);
            
        } catch (HttpException $ex) {
            // TODO: add error handlers
            $response = new Response($ex->getMessage(), [], $ex->getCode());
            
        } catch (AuthenticationRequiredException $ex) {
            /* @var $authenticator Security\Authentication\AuthenticatorInterface */
            $authenticator = $this->container->get('@authenticator');
            $response = $authenticator->onFailure($request, $ex);
            
        } catch (\Throwable $ex) {
            $message = $ex->getFile() . ':' . $ex->getLine() . "<br />\n"
                . $ex->getMessage() . "<br />\n"
                . '<pre>' . $ex->getTraceAsString() . '</pre>';

            $response = new Response($message, [], 500);
            
        } finally {
            ob_end_clean();
        }
        
        return $response;
    }

    /**
     * 
     * @param ResponseInterface $response
     * @return void
     */
    public function send(ResponseInterface $response): void
    {
        
        http_response_code($response->getCode());
        foreach ($response->getHeaders() as $header => $value) {
            header("$header: $value", true);
        }
        echo $response->getContent();
    }
    
}
