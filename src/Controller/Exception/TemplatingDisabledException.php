<?php

namespace Cheddar\Controller\Exception;

class TemplatingDisabledException extends \RuntimeException
{
    
    protected $message = 'Templating engine is disabled or not configured';
    
}
