<?php

namespace Cheddar\Controller;

use Cheddar\DependencyInjection\{Container, Resolver};
use Cheddar\Http\Request;
use Cheddar\Router\Route;

class ControllerResolver
{
    /**
     *
     * @var ContainerInterface
     */
    protected $container;
    
    /**
     *
     * @var Resolver
     */
    protected $resolver;
    
    public function __construct(Container $container, Resolver $resolver)
    {
        $this->container = $container;
        $this->resolver = $resolver;
    }

    /**
     * 
     * @param Request $request
     * @param Route $route
     * @return callable
     */
    public function resolve(Route $route): array
    {
        $controller = $route->getController();
        $instance = $this->resolver->getInstance($controller);
        
        $method = new \ReflectionMethod($instance, $route->getAction());
        $args = $this->resolver->getArguments($method, $route->getParameters());
        
        return [[$instance, $route->getAction()], $args];
    }
    
}