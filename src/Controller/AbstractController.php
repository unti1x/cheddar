<?php

namespace Cheddar\Controller;

use Cheddar\DependencyInjection\Container;
use Cheddar\Controller\Exception\TemplatingDisabledException;
use Cheddar\Http\{Response, JsonResponse, RedirectResponse};

abstract class AbstractController
{
    
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }
    
    protected function response(string $content, int $code = Response::HTTP_OK): Response
    {
        return new Response($content, [], $code);
    }
 
    protected function json(array $data, int $code = JsonResponse::HTTP_OK): JsonResponse
    {
        return new JsonResponse($data, [], $code);
    }
    
    protected function redirect(string $url, int $code = RedirectResponse::HTTP_FOUND): RedirectResponse
    {
        return new RedirectResponse($url, $code);
    }
    
    protected function render(string $template, array $arguments = []): Response
    {
        if(!$this->container->has('@templating')) {
            throw new TemplatingDisabledException();
        }
        
        /* @var $templating Cheddar\Templating\TemplateEngineInterface */
        $templating = $this->container->get('@templating');
        
        if($this->container->has('@user')) {
            $arguments['user'] = $this->container->get('@user');
        }

        $data = $templating->render($template, $arguments);
        
        return $this->response($data);
    }
    
}
