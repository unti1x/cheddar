<?php

namespace Cheddar\Templating;

/**
 * @author unti1x
 */
class PhpTemplateEngine implements TemplateEngineInterface
{
    
    protected string $templatesDir;
    
    public function __construct(array $_configuration = [])
    {
        $this->templatesDir = $_configuration['templatesDir'];
    }
    
    public function escape(string $text): string
    {
        return nl2br(htmlspecialchars($text));
    }

    public function render(string $template, array $arguments = array()): string
    {
        ob_start();
        
        (function($_template, $_args) {
            extract($_args);
            unset($_args);
            $view = $this;
            include $this->templatesDir . '/' . $_template;
        }) ($template, $arguments);
        
        return ob_get_clean();
    }
    
}
