<?php

namespace Cheddar\Templating;

/**
 *
 * @author unti1x
 */
interface TemplateEngineInterface
{
    
    public function render(string $template, array $arguments = []): string;
    
}
