<?php

namespace Cheddar\ParameterBag;

class ParameterBag implements \ArrayAccess
{
    
    /**
     *
     * @var array
     */
    protected $parameters;
    
    /**
     * 
     * @param array $parameters
     */
    public function __construct(?array $parameters = [])
    {
        $this->parameters = $parameters;
    }
    
    /**
     * @todo Add dot-path accessor
     * @param string $name
     * @return mixed
     */
    public function get(string $name)
    {
        return $this[$name];
    }
    
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
    }
    
    public function all(): array
    {
        return $this->parameters;
    }
    
    // ArrayAccess
    
    public function offsetExists($offset): bool
    {
        return isset($this->parameters[$offset]) || array_key_exists($offset, $this->parameters);
    }

    public function offsetGet($offset)
    {
        return $this->offsetExists($offset) ? $this->parameters[$offset] : null;
    }
    
    public function offsetSet($offset, $value): void
    {
        // TODO: use dot-path accessor
        $this->parameters[$offset] = $value;
    }

    public function offsetUnset($offset): void
    {
        // TODO: use dot-path accessor
        unset($this->parameters[$offset]);
    }

}
