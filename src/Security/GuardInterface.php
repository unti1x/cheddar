<?php

namespace Cheddar\Security;

/**
 *
 * @author unti1x
 */
interface GuardInterface
{
    
    public function getUser(): ?User\UserInterface;
    
}
