<?php

namespace Cheddar\Security\User;

use Cheddar\DBAL\RepositoryLocator;
use Cheddar\Security\Exception\UserNotFoundException;

class DatabaseProvider implements UserProviderInterface
{
    
    private string $userClass;
    
    /**
     * @todo throw a proper exception instead of relying on type hinting
     * @var UserRepositoryInterface 
     */
    private UserRepositoryInterface $repo;
    
    public function __construct(RepositoryLocator $locator, array $_configuration)
    {
        $this->userClass = $_configuration['userClass'];
        $this->repo = $locator->getRepository($this->userClass);
    }
    
    public function loadByUsername(string $username): UserInterface
    {
        $user = $this->repo->findByUsername($username);
        if(!$user) {
            throw new UserNotFoundException();
        }
        
        return $user;
    }
    
}
