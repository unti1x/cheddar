<?php

namespace Cheddar\Security\User;

interface UserInterface
{
    
    /**
     * Get user's display name
     * 
     * @return string
     */
    public function getUsername(): string;
    
    /**
     * Get password
     * 
     * @return string
     */
    public function getPasswordHash(): string;
    
}
