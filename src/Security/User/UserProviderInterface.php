<?php

namespace Cheddar\Security\User;

/**
 *
 * @author unti1x
 * @todo add supportsClass and refreshUser methods
 */
interface UserProviderInterface
{
    
    /**
     * Load user for the given username
     * 
     * @return \Cheddar\Security\UserInterface
     * @throws \Cheddar\Security\Exception\UserNotFoundException
     */
    public function loadByUsername(string $username): UserInterface;

}
