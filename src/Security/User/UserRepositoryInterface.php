<?php

namespace Cheddar\Security\User;

use Cheddar\Security\User\UserInterface;

/**
 *
 * @author unti1x
 */
interface UserRepositoryInterface
{
    
    public function findByUsername(string $username): ?UserInterface;
    
}
