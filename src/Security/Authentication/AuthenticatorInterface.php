<?php

namespace Cheddar\Security\Authentication;

use Cheddar\Http\{RequestInterface, ResponseInterface};
use Cheddar\Security\Exception\AuthenticationException;
use Cheddar\Security\User\{UserProviderInterface, UserInterface};

/**
 *
 * @author unti1x
 * @todo add supports check and rememberMe
 */
interface AuthenticatorInterface
{
    /**
     * 
     * @param RequestInterface $request
     * @return mixed
     */
    public function getCredentials(RequestInterface $request);
    
    /**
     * 
     * @param mixed $credentials
     * @param \Cheddar\Security\UserProviderInterface $userProvider
     * @return \Cheddar\Security\UserInterface
     */
    public function getUser($credentials, UserProviderInterface $userProvider): UserInterface;
    
    /**
     * 
     * @param mixed $credentials
     * @param \Cheddar\Security\UserInterface $user
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user): bool;
    
    /**
     * 
     * @param RequestInterface $request
     * @param AuthenticationException $exception
     * @return ResponseInterface
     */
    public function onFailure(RequestInterface $request, AuthenticationException $exception): ResponseInterface;

    /**
     * 
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function onSuccess(RequestInterface $request): ResponseInterface;
    
}
