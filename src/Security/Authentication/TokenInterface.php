<?php

namespace Cheddar\Security\Authentication;

/**
 * @author unti1x
 * @todo add roles support
 */
interface TokenInterface
{
    /**
     * 
     * @return \Cheddar\Security\UserInterface
     */
    public function getUser(): ?UserInterface;
    
    /**
     * 
     * @param \Cheddar\Security\UserInterface $user
     */
    public function setUser(UserInterface $user): void;
    
    /**
     * 
     * @return bool
     */
    public function isAuthenticated(): bool;
    
    /**
     * 
     * @param bool $isAuthenticated
     * @return void
     */
    public function setAuthenticated(bool $isAuthenticated): void;
    
}
