<?php

namespace Cheddar\Security\Authentication;


class BasicCredentialsEncoder implements CredentialsEncoderInterface
{
    
    public function isValid(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }
    
    public function encode(string $password): string
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }
    
}
