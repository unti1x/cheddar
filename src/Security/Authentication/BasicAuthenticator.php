<?php

namespace Cheddar\Security\Authentication;

use Cheddar\Http\{RequestInterface, RedirectResponse, ResponseInterface};
use Cheddar\Security\User\{UserProviderInterface, UserInterface};
use Cheddar\Security\Exception\AuthenticationException;

/**
 *
 * @author unti1x
 */
class BasicAuthenticator implements AuthenticatorInterface
{
    protected const DEFAULT_USERNAME = 'email';
    
    protected const DEFAULT_PASSWORD = 'password';

    protected const DEFAULT_LOGIN_PAGE = '/login';

    protected const DEFAULT_SUCCESS_PAGE = '/';
    
    protected string $usernameField;
    
    protected string $passwordField;
    
    protected string $loginPage;
    
    protected string $successPage;
    
    protected CredentialsEncoderInterface $encoder;

    public function __construct(
        BasicCredentialsEncoder $encoder,
        array $_configuration = []
    )
    {
        $this->usernameField = $_configuration['usernameField'] ?? self::DEFAULT_USERNAME;
        $this->passwordField = $_configuration['passwordField'] ?? self::DEFAULT_PASSWORD;
        $this->successPage = $_configuration['successPage'] ?? self::DEFAULT_SUCCESS_PAGE;
        $this->loginPage = $_configuration['loginPage'] ?? self::DEFAULT_LOGIN_PAGE;
        $this->encoder = $encoder;
    }
    
    public function checkCredentials($credentials, UserInterface $user): bool
    {
        $password = $credentials['password'];
        $hash = $user->getPasswordHash();
        return $this->encoder->isValid($password, $hash);
    }

    public function getCredentials(RequestInterface $request)
    {
        $params = $request->getRequest();
        return [
            'username' => $params->get($this->usernameField),
            'password' => $params->get($this->passwordField)
        ];
    }

    protected function getUsername($credentials): string
    {
        return $credentials['username'];
    }
    
    public function getUser($credentials, UserProviderInterface $userProvider): UserInterface
    {
        $username = $this->getUsername($credentials);
        return $userProvider->loadByUsername($username);
    }

    public function onFailure(RequestInterface $request, AuthenticationException $exception): ResponseInterface
    {
        return new RedirectResponse($this->loginPage);
    }

    public function onSuccess(RequestInterface $request): ResponseInterface
    {
        return new RedirectResponse($this->successPage);
    }

    
}
