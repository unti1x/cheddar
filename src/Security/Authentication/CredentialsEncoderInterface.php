<?php

namespace Cheddar\Security\Authentication;

/**
 * @author unti1x
 */
interface CredentialsEncoderInterface
{
    
    public function isValid(string $password, string $hash): bool;
    
    public function encode(string $password): string;
    
}
