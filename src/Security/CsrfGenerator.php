<?php

namespace Cheddar\Security;

/**
 * Description of CsrfGenerator
 *
 * @author unti1x
 */
class CsrfGenerator
{
    private const TTL = 60 * 15;
    
    private string $secret;
    
    public function __construct(array $_configuration)
    {
        $this->secret = $_configuration['secret'];
    }
    
    public function generateToken(int $timestamp = null): string
    {
        if(!$timestamp) {
            $timestamp = time();
        }
        
        $data = $timestamp . session_id();
        $token = hash_hmac('haval192,3', $data, $this->secret);
        
        return "$token.$timestamp";
    }
    
    public function validate(string $token): bool
    {
        [$_, $timestamp] = explode('.', $token);
        $newToken = $this->generateToken($timestamp);

        return ($token === $newToken) && (time() - (int)$timestamp) < self::TTL;
    }
}
