<?php

namespace Cheddar\Security;

use Cheddar\Security\User\UserInterface;
use Cheddar\Security\User\UserProviderInterface;

/**
 * @author unti1x
 */
class Guard implements GuardInterface
{
    
    private ?UserInterface $user = null;
    
    private UserProviderInterface $userProvider;
    
    public function __construct(UserProviderInterface $userProvider)
    {
        $this->userProvider = $userProvider;
    }
    
    public function start(?UserInterface $user = null): void
    {
        session_start();
        
        if($user) {
            $this->user = $user;
            $this->setSessionUsername($user->getUsername());
            
        } elseif($this->isSessionAuthenticated()) {
            $this->user = $this->userProvider->loadByUsername($this->getSessionUsername());
            
        }
    }
    
    public function logout()
    {
        unset($_SESSION['_auth_username']);
        $this->user = null;
    }
    
    public function commit()
    {
        session_write_close();
    }
    
    public function isStarted(): bool
    {
        return session_status() === PHP_SESSION_ACTIVE;
    }
    
    public function isSessionAuthenticated()
    {
        return isset($_SESSION['_auth_username']);
    }
    
    public function getSessionUsername()
    {
        return $_SESSION['_auth_username'];
    }
    
    public function setAuthenticated()
    {
        $this->authenticated = true;
    }
    
    public function setSessionUsername(string $username)
    {
        $_SESSION['_auth_username'] = $username;
    }
    
    public function getUser(): ?UserInterface
    {
        return $this->user;
    }
    
}
