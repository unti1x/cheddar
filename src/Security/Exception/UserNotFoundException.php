<?php

namespace Cheddar\Security\Exception;

class UserNotFoundException extends AuthenticationException
{
    protected $message = 'User not found';
}
