<?php

namespace Cheddar\Security\Exception;

/**
 * @author unti1x
 */
class AuthenticationRequiredException extends AuthenticationException
{
    protected $message = 'Authentication required';
}
