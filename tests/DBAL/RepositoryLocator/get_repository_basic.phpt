--TEST--
RepositoryLocator::getRepository() basic test
--FILE--
<?php
require __DIR__ . '/../../bootstrap.php';
$schema = require( __DIR__ . '/../fixture_schema.php');

// FIXME: mock
$em = new \Cheddar\DBAL\EntityManager($schema);

$hydratorMock = new class($schema) extends Cheddar\DBAL\Hydrator {
    public function hydrate(\Cheddar\DBAL\QuerySchema $schema, $rawData): array
    {
        echo $schema->getEntity() . "\n" . $schema->getSql() . "\n";
        return [];
    }
};

$connectionMock = new class extends Cheddar\DBAL\Connection {
    public function __construct() {}
    public function execute(string $sql, array $parameters = array(), bool $returnResult = true)
    {
        echo "Query executed\n";
    }
};

$adapter = new Cheddar\DBAL\DatabaseAdapter($connectionMock, $schema, $hydratorMock);

$locator  = new Cheddar\DBAL\RepositoryLocator($schema, $em, $adapter);
echo get_class($locator->getRepository(Foo::class)) . "\n";
echo get_class($locator->getRepository(Bar::class)) . "\n";
?>

--EXPECT--
FooRepository
Cheddar\DBAL\SimpleRepository
