--TEST--
Hydrator::hydrate() basic test
--FILE--
<?php
require __DIR__ . '/../../bootstrap.php';
$schema = require( __DIR__ . '/../fixture_schema.php');

$query = new Cheddar\DBAL\QuerySchema();
$query
    ->setEntity(Foo::class)
    ->setTable('foo')
    ->setFields(['id', 'bar_id'])
    ->addRelation(
        'bar', 
        'bar',
        Cheddar\DBAL\Query::JOIN_LEFT ,
        $schema->get(Foo::class)['relations']['bar'], 
        ['id', 'baz']
    );

$data = [
    [1, 1, 1, 'quuz'],
    [2, 1, 1, 'quuz'],
    [3, 1, 1, 'quuz'],
    [4, 2, 2, 'quuuz'],
    [5, 2, 2, 'quuuz']
];

$hydrator = new Cheddar\DBAL\Hydrator($schema);
/* @var $entities Foo[] */
$entities = $hydrator->hydrate($query, $data);
$objects = ['foo' => [], 'bar' => []];
foreach ($entities as $entity) {
    $objects['foo'][] = spl_object_hash($entity);
    $objects['bar'][] = spl_object_hash($entity->getBar());
}

var_dump(count(array_unique($objects['foo'])));
var_dump(count(array_unique($objects['bar'])));

?>

--EXPECT--
int(5)
int(2)
