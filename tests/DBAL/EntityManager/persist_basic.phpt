--TEST--
EntityManager::persist() basic test
--FILE--
<?php
require __DIR__ . '/../../bootstrap.php';
$schema = require( __DIR__ . '/../fixture_schema.php');

$manager = new \Cheddar\DBAL\EntityManager($schema);
$foo = new Foo();
$bar = new Bar();
$foo->setBar($bar);

$manager->persist($foo);
var_dump($manager->getStatus($bar));
?>

--EXPECT--
int(1)
