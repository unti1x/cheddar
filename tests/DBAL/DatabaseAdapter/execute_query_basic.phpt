--TEST--
DatabaseAdapter::executeQuery() basic test
--FILE--
<?php
require __DIR__ . '/../../bootstrap.php';
$schema = require( __DIR__ . '/../fixture_schema.php');

$hydratorMock = new class($schema) extends Cheddar\DBAL\Hydrator {
    public function hydrate(\Cheddar\DBAL\QuerySchema $schema, $rawData): array
    {
        echo $schema->getEntity() . "\n" . $schema->getSql() . "\n";
        return [];
    }
};

$connectionMock = new class extends Cheddar\DBAL\Connection {
    public function __construct() {}
    public function execute(string $sql, array $parameters = array(), bool $returnResult = true)
    {
        echo "Query executed\n";
    }
};

$query = (new \Cheddar\DBAL\Query())
    ->select(Foo::class)
    ->with('bar')
    ->filter('`bar`.`baz` != :quuz')
    ->sortBy('id', 'DESC')
    ->limit(10, 15);

$adapter = new Cheddar\DBAL\DatabaseAdapter($connectionMock, $schema, $hydratorMock);
$adapter->executeQuery($query);
?>

--EXPECT--
Query executed
Foo
SELECT `foo`.`id`, `foo`.`bar_id`, `bar`.`id`, `bar`.`baz` FROM `foo` LEFT JOIN `bar` `bar` ON `bar`.`id` = `foo`.`bar_id` WHERE `bar`.`baz` != :quuz ORDER BY id DESC LIMIT 10 OFFSET 15
