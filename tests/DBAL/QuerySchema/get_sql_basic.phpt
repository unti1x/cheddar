--TEST--
QuerySchema::getSql() basic test
--FILE--
<?php
require __DIR__ . '/../../bootstrap.php';

$query = new Cheddar\DBAL\QuerySchema();
$query->setTable('foo')
    ->setFields(['id', 'bar_id'])
    ->addRelation('bar', 'bar', Cheddar\DBAL\Query::JOIN_LEFT ,[
        'local' => 'bar_id',
        'foreign' => 'id'
    ], ['id', 'baz'])
    ->setConditions('`bar`.`baz` != :quuz')
    ->setLimit(10)
    ->setSort(['id' => 'DESC'])
    ->setSkip(15);

var_dump($query->getSql());
var_dump($query->getFieldsMap());
var_dump($query->getRelations());
var_dump($query->getFields());

?>

--EXPECT--
string(185) "SELECT `foo`.`id`, `foo`.`bar_id`, `bar`.`id`, `bar`.`baz` FROM `foo` LEFT JOIN `bar` `bar` ON `bar`.`id` = `foo`.`bar_id` WHERE `bar`.`baz` != :quuz ORDER BY id DESC LIMIT 10 OFFSET 15"
array(2) {
  ["_self"]=>
  array(2) {
    ["id"]=>
    int(0)
    ["bar_id"]=>
    int(1)
  }
  ["bar"]=>
  array(2) {
    ["id"]=>
    int(2)
    ["baz"]=>
    int(3)
  }
}
array(1) {
  ["bar"]=>
  array(4) {
    ["local"]=>
    string(6) "bar_id"
    ["foreign"]=>
    string(2) "id"
    ["table"]=>
    string(3) "bar"
    ["join"]=>
    string(4) "left"
  }
}
array(4) {
  [0]=>
  string(10) "`foo`.`id`"
  [1]=>
  string(14) "`foo`.`bar_id`"
  [2]=>
  string(10) "`bar`.`id`"
  [3]=>
  string(11) "`bar`.`baz`"
}