<?php
class Bar {
    private ?int $id = null;
    
    public function getId(): ?int
    {
        return $this->id;
    }
}

class Foo {
    private ?Bar $bar = null;

    public function setBar(?Bar $bar): self
    {
        $this->bar = $bar;
        return $this;
    }
    
    public function getBar(): ?Bar
    {
        return $this->bar;
    }
}

class FooRepository extends \Cheddar\DBAL\AbstractRepository {}

return new Cheddar\DBAL\Schema([
    Foo::class => [
        'table' => 'foo',
        'repository' => FooRepository::class,
        'fields' => [
            'id' => [ 'type' => 'int' ],
            'bar_id' => [ 'type' => 'int' ]
        ],
        'relations' => [
            'bar' => [
                'target' => Bar::class,
                'local' => 'bar_id',
                'foreign' => 'id',
                'type' => 'one',
                'cascade' => ['persist']
            ]
        ]
    ],
    Bar::class => [
        'table' => 'bar',
        'fields' => [
            'id' => [ 'type' => 'int' ],
            'baz'=> [ 'type' => 'text' ]
        ]
    ]
]);
