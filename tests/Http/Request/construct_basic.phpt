--TEST--
Request::__construct basic test
--EXTENSIONS--
json
--SKIPIF--
<?php if (php_sapi_name()=='cli') die('skip'); ?>
--POST_RAW--
Content-Type: application/json

{"test":"ok"}
--FILE--
<?php
require __DIR__ . '/../../bootstrap.php';

$request = new Cheddar\Http\Request(
    $_GET, 
    $_POST,
    $_SERVER
);
var_dump($request->getRequest()->get('test'));

?>

--EXPECT--
string(2) "ok"
