--TEST--
Kernel::handleRequest() basic test

--FILE--
<?php
require __DIR__ . '/../bootstrap.php';

use Cheddar\Controller\AbstractController;
use Cheddar\Router\RouteMatcher;
use Cheddar\Security\User\{UserProviderInterface, UserInterface};
use Cheddar\Http\{Request, Response};

class FooController extends AbstractController
{
    public function bar()
    {
        return new Response('baz');
    }
}


$kernel = new class extends \Cheddar\Kernel {
    public function loadParameters() {
        $this->parameters->setParameters([
            RouteMatcher::class => [
                [
                    'route' => '%^/%',
                    'action' => [FooController::class, 'bar']
                ]
            ],
            'services' => [
                UserProviderInterface::class => new class implements UserProviderInterface {
                    public function loadByUsername($username): UserInterface {}
                },
            ],
            'parameters' => []
        ]);
    }
};

$kernel->boot();
$request = new Request([], [], ['REQUEST_URI' => '/', 'REQUEST_METHOD' => 'GET']);
$response = $kernel->handleRequest($request);
var_dump($response->getContent());
?>

--EXPECT--
string(3) "baz"
