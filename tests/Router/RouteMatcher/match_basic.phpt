--TEST--
RouteMatcher::match basic test
--FILE--
<?php
require __DIR__ . '/../../bootstrap.php';

$request = new Cheddar\Http\Request([], [], [
    'REQUEST_URI' => '/foo/bar/baz',
    'REQUEST_METHOD' => 'GET'
]);

$routes = [
    [
        'route' => '%^/foo/bar/(?P<tail>\w+)%',
        'action' => ['FooBarController', 'quuz'],
        'methods' => ['GET']
    ]
];
    
$router = new \Cheddar\Router\RouteMatcher($routes);
$route = $router->match($request);

var_dump($route->getController());
var_dump($route->getAction());
var_dump($route->getParameters()->get('tail'));

?>

--EXPECT--
string(16) "FooBarController"
string(4) "quuz"
string(3) "baz"
