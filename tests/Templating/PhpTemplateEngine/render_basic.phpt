--TEST--
PhpTemplatingEngine::render() basic test
--FILE--
<?php
require __DIR__ . '/../../bootstrap.php';

$engine = new Cheddar\Templating\PhpTemplateEngine([
    'templatesDir' => __DIR__
]);
echo $engine->render('template.php', [
    'message' => 'Mess with the best, die like the rest'
]);
?>

--EXPECT--
Mess with the best, die like the rest
Variables: 3
