--TEST--
ConfigurationLoader::load() basic test

--FILE--
<?php
require __DIR__ . '/../../bootstrap.php';

$config = \Cheddar\DependencyInjection\ConfigurationLoader::load(__DIR__.'/fixture.php');
var_dump($config->get('test'));
?>

--EXPECT--
string(2) "ok"
