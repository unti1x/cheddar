--TEST--
Container::get() basic test

--FILE--
<?php
require __DIR__ . '/../../bootstrap.php';

interface DependencyInterface {}

class Dependency implements DependencyInterface {}

class TestService {
    public $d;
    public function __construct(DependencyInterface $d) {
        $this->d = $d;
    }
}

$config = new Cheddar\ParameterBag\ParameterBag([
    'parameters' => [],
    'services' => [
        DependencyInterface::class => Dependency::class,
        '@test_service' => TestService::class
    ]
]);

$container = new Cheddar\DependencyInjection\Container($config);
$resolver = new Cheddar\DependencyInjection\Resolver($container);
$container->setResolver($resolver);

$service = $container->get('@test_service');
var_dump(get_class($service));
var_dump(get_class($service->d));

?>

--EXPECT--
string(11) "TestService"
string(10) "Dependency"
